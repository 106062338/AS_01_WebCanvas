
var _canvas;
var ctx;
var rect; 
var tool="pencil";
var curtype="Path";
var painterWidth=2;
var textWidth = 1;
var graphicsA = [], graphicsB = [];
var points = [];
var color, paletteColor;
var start, alreadydown=0;
var inputText="";
var font = "25px Arial";
var fontSize, fontFamily = "Arial";
var eraserWidth = 10, eraserColor = "white";
var gradient;
function init() {
    // initialisation stuff here
    _canvas = document.getElementById("myCanvas");
    ctx = _canvas.getContext('2d'); 
    ctx.save(); 
    console.log(ctx.lineWidth);
    color = document.getElementById("palette").value;
    draw();
    //console.log(document.body.style.cursor);
    document.body.style.cursor = 'url("icons/pencil.png"), auto';
    fontSize = (document.getElementById("fontSize").value)*5;
    var rect = _canvas.getBoundingClientRect();
    gradient=ctx.createLinearGradient(rect.left,rect.bottom,rect.left,rect.top);
    gradient.addColorStop(0,'red');
    gradient.addColorStop(0.3,'orange');
    gradient.addColorStop(0.36,'yellow');
    gradient.addColorStop(0.54,'green');
    gradient.addColorStop(0.69,'blue');
    gradient.addColorStop(0.8,'rgb(51, 51, 204)');
    gradient.addColorStop(1,'purple');



}
class Shape{
    constructor(type, color, lineWidth) {
        this.type = type;
        this.strokeStyle = color;
        this.lineWidth = lineWidth; 
    }
}
class Point{
    constructor(x, y){
        this.x = x;
        this.y = y;
    }
}
class Path extends Shape{
    constructor(type, color, lineWidth, points) {
        super(type, color, lineWidth);
        this.points = points; 
    }
}
class Polygon extends Shape{
    constructor(type, color, lineWidth, startPoint, endPoint) {
        super(type, color, lineWidth);
        this.startPoint = startPoint;
        this.endPoint = endPoint; 
    }
}
class Text extends Shape{
    constructor(type, color, lineWidth, startPoint, font, inputString) {
        super(type, color, lineWidth);
        this.startPoint = startPoint;
        this.font = font;
        this.inputString = inputString;
    }
}
class newString{
    constructor(string)
    {
        this.string = string;
    }
}
$(document).ready(function(){

    $("#eraser").click(function(){
        tool = "eraser";
        curtype = "Path";
        document.body.style.cursor = 'url("icons/eraser-cursor.png"), auto';
        //console.log(document.body.style.cursor);
        //painterWidth = 10;
        //color="white";
     });

    //  $("#paint-brush").click(function(){
    //     tool = "paint-brush";
    //  });
     $("#palette").change(function(event){
        color = event.target.value;
        //paletteColor = event.target.value;
     });
     $("#fontSize").change(function(event)
     {
        fontSize = (event.target.value)*5;
        font = fontSize + "px " + fontFamily;
     })
     $("select.fonts").change(function(){
        var temp = fontSize + "px" + " " + $(this).children("option:selected").val();
        var choice = $(this).children("option:selected").val();
        
        //document.body.style.cursor = 'default';
        //console.log(document.body.style.cursor);
        switch(choice)
        {
            case "Arial":
                fontFamily = "Arial";
            break;
            case "Fantasy":
                fontFamily = "fantasy";
            break;
            case "Lucida Console":
                fontFamily = "cursive";
            break;
        }
        font = fontSize + "px " + fontFamily;
    });

    $("#download").click(function(e){
        var image = _canvas.toDataURL("image/png");
        var link = document.createElement('a');
        link.download = "my-image.png";
        link.href = image;
        link.click();
    });

    $("#trash").click(function(e){
        graphicsA=[];
        graphicsB=[];
        redraw();
    });

    $("#redo").click(function(e){
        if(graphicsB.length!=0)
        {
            graphicsA.push(graphicsB[graphicsB.length-1]);
            graphicsB.pop();
            redraw();
        }
    });

    $("#undo").click(function(e){
        if(graphicsA.length!=0)
        {
            graphicsB.push(graphicsA[graphicsA.length-1]);
            graphicsA.pop();
            redraw();
        }
    });
    

    $("#rectangle").click(function(e){
        curtype = "Rectangle";
        document.body.style.cursor = 'url("icons/rectangle-cur.png"), auto';
    });
    $("#circle").click(function(e){
        curtype = "Circle";
        document.body.style.cursor = 'url("icons/circle-cur.png"), auto';

    });
    $("#triangle").click(function(e){
        curtype = "Triangle";
        document.body.style.cursor = 'url("icons/triangle-cur.png"), auto';
    });

    $("#text").click(function(){
        curtype = "Text";
        document.body.style.cursor = "text";
    })
    $("select.painters").change(function(){
        tool = $(this).children("option:selected").val();
        curtype = "Path";
        switch(tool)
        {
            case "marker":
                document.body.style.cursor = 'url("icons/marker.png"), auto';
                painterWidth=5;
            break;
            case "brush":
                document.body.style.cursor = 'url("icons/brush.png"), auto';            
                painterWidth=15;
            break;

            case "pencil":
                document.body.style.cursor = 'url("icons/pencil.png"), auto';
                painterWidth=2;
            break;
        }
    });
    $("#rainbow").click(function(){
        curtype = "Path";
        tool = "rainbow";
    });
});
function redraw()
{
    clear();
    console.log(graphicsA);
    var i;
    for(i=0;i<graphicsA.length;i++)
    {
        switch(graphicsA[i].type)
        {
            case "Path":
                drawPath(graphicsA[i]);
            break;
            case "Rectangle":
                drawRectangle(graphicsA[i]);
            break;
            case "Circle":
                drawCircle(graphicsA[i]);
            break;
            case "Triangle":
                drawTriangle(graphicsA[i]);
            break;
            case "Text":
                drawText(graphicsA[i]);
            break;
            default:
                ctx.drawImage(graphicsA[i],0,0);

        }
    }
}
function draw(){
    
    _canvas.addEventListener("mousedown", function(evt){
        var mousePos = getMousePos(evt);
        if(alreadydown!=1)
        {
            start = new Point(mousePos.x, mousePos.y);
            //alreadydown = 1;
        }
        var testString = new newString("test");
        switch(curtype)
        {
            case "Path":
                points.push(mousePos.x, mousePos.y);
                if(tool=="eraser")
                {
                    graphicsA.push(new Path(curtype, eraserColor, eraserWidth, points));
                }
                else if (tool=="rainbow")
                {
                    graphicsA.push(new Path(curtype, gradient, painterWidth, points));
                }
                else
                {
                    graphicsA.push(new Path(curtype, color, painterWidth, points));
                }
                points=[];
            break;
            case "Rectangle":
                graphicsA.push(new Polygon(curtype, color, painterWidth, start, start));
                //alert("new!");
            break;
            case "Triangle":
                graphicsA.push(new Polygon(curtype, color, painterWidth, start, start));
                //alert("new!");
            break;
            case "Circle":
                graphicsA.push(new Polygon(curtype, color, painterWidth, start, start));   
            break;
            case "Text":
                graphicsA.push(new Text(curtype, color, textWidth, new Point(mousePos.x, mousePos.y), font, ""));
                readyType = 1;
            break;
        }
        if(curtype!="Text")
        {
            _canvas.addEventListener('mousemove', mouseMove, false);
        }

    });

    document.addEventListener('keypress', typing, false);
    _canvas.addEventListener('mouseup', function(evt) {
        _canvas.removeEventListener('mousemove', mouseMove, false);
        var mousePos = getMousePos(evt);
        alreadydown = 0;

        switch(curtype)
        {
            case "Path":
            graphicsA[graphicsA.length-1].points.push(new Point(mousePos.x, mousePos.y));
            //points=[];
            redraw();    
            break;
            case "Rectangle":
                alreadydown = 0;
                graphicsA[graphicsA.length-1].endPoint=(new Point(mousePos.x, mousePos.y));
                redraw();
            break;
            case "Circle":
                alreadydown = 0;
                graphicsA[graphicsA.length-1].endPoint=(new Point(mousePos.x, mousePos.y));
                redraw();
            break;
            case "Triangle":
                alreadydown = 0;
                graphicsA[graphicsA.length-1].endPoint=(new Point(mousePos.x, mousePos.y));
                redraw();
            break;
        }
        
      }, false);
}
function typing(evt)
{
    if(curtype=="Text")
    {
        var input = String.fromCharCode(evt.keyCode);
        console.log(input);
        graphicsA[graphicsA.length-1].inputString = graphicsA[graphicsA.length-1].inputString.toString()+input;
        redraw();
    }
}


function mouseMove(evt) {
    //console.log(graphicsA);
    var mousePos = getMousePos(evt);
    switch(curtype)
    {
        case "Path":
            graphicsA[graphicsA.length-1].points.push(new Point(mousePos.x, mousePos.y));
        break;
        case "Rectangle":
            graphicsA[graphicsA.length-1].endPoint = new Point(mousePos.x, mousePos.y);
        break;
        case "Circle":
            graphicsA[graphicsA.length-1].endPoint = new Point(mousePos.x, mousePos.y);
        break;
        case "Triangle":
            graphicsA[graphicsA.length-1].endPoint = new Point(mousePos.x, mousePos.y);
        break;
    }
    //console.log(points);
    redraw();
}

function drawText(text)
{
    //ctx.restore();
    ctx.font = text.font;
    console.log(ctx.font);
    ctx.strokeStyle = text.strokeStyle;
    ctx.lineWidth=1;
    console.log(ctx.lineWidth);
    if(text.inputString!="")
    {
        ctx.strokeText(text.inputString.toString(), text.startPoint.x, text.startPoint.y);
    }
}
function drawPath(path)
{
    var i;
    ctx.beginPath();
    ctx.lineCap='round';
    ctx.lineJoin='round';
    ctx.moveTo(path.points[0].x, path.points[0].y);
    ctx.lineWidth = path.lineWidth;
    ctx.strokeStyle = path.strokeStyle;
    for(i=0;i<path.points.length;i++)
    {
        ctx.lineTo(path.points[i].x, path.points[i].y);
        ctx.stroke();
    }
}
function drawTriangle(triangle)
{
    ctx.beginPath();
    ctx.moveTo(triangle.startPoint.x, triangle.startPoint.y);
    ctx.lineWidth = triangle.lineWidth;
    ctx.strokeStyle = triangle.strokeStyle;
    if((triangle.startPoint.x-triangle.endPoint.x!=0)&&(triangle.startPoint.y-triangle.endPoint.y!=0))
    {
        ctx.lineTo(triangle.endPoint.x, triangle.endPoint.y);
        ctx.stroke();
        ctx.lineTo(2*(triangle.startPoint.x)-triangle.endPoint.x, triangle.endPoint.y);
        ctx.stroke();
        ctx.lineTo(triangle.startPoint.x, triangle.startPoint.y);
        ctx.stroke();
    }

}
function drawCircle(circle)
{
    ctx.beginPath();
    ctx.lineWidth = circle.lineWidth;
    ctx.strokeStyle = circle.strokeStyle;
    ctx.arc(0.5*(circle.startPoint.x + circle.endPoint.x), 0.5*(circle.startPoint.y + circle.endPoint.y), dist(circle.startPoint,circle.endPoint), 0, 2 * Math.PI);
    ctx.stroke();
}
function drawRectangle(rectangle)
{
    ctx.beginPath();
    ctx.moveTo(rectangle.startPoint.x, rectangle.startPoint.y);
    ctx.lineWidth = rectangle.lineWidth;
    ctx.strokeStyle = rectangle.strokeStyle;
    if((rectangle.startPoint.x-rectangle.endPoint.x!=0)&&(rectangle.startPoint.y-rectangle.endPoint.y!=0))
    {
        ctx.lineTo(rectangle.startPoint.x, rectangle.endPoint.y);
        ctx.stroke();
        ctx.lineTo(rectangle.endPoint.x, rectangle.endPoint.y);
        ctx.stroke();
        ctx.lineTo(rectangle.endPoint.x, rectangle.startPoint.y);
        ctx.stroke();
        ctx.lineTo(rectangle.startPoint.x, rectangle.startPoint.y);
        ctx.stroke();
    }

}
function clear()
{
    ctx.clearRect (0, 0, _canvas.width, _canvas.height);
}
function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            _canvas.width = img.width;
            _canvas.height = img.height;
            graphicsA.push(img);
            redraw();
            //ctx.drawImage(img,0,0);
            //console.log("ready");
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}

function dist(p1, p2)
{
    return Math.sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y));
}
//Get Mouse Position
function getMousePos(e) {
    rect = _canvas.getBoundingClientRect();
    return {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };
}